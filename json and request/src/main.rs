use reqwest::Client;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct ToDo {
    id: Option<i32>,
    #[serde(rename = "userId")]
    user_id: i32,
    title: String,
    completed: bool
}

#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    // Send request and get data...
    let str_todos = Client::new()
        .get("https://jsonplaceholder.typicode.com/todos?userId=1")
        .send()
        .await?
        .text()
        .await?;

    println!("{:#?}", str_todos);

    // Get json response to struct
    let json_todos: Vec<ToDo> = Client::new()
        .get("https://jsonplaceholder.typicode.com/todos?userId=1")
        .send()
        .await?
        .json()
        .await?;

    println!("{:#?}", json_todos);

    // Send json request with struct
    let new_todo = ToDo {
        id: None,
        user_id: 1,
        title: "Hello world!".to_owned(),
        completed: false
    };
    let create_todo: ToDo = Client::new()
        .post("https://jsonplaceholder.typicode.com/todos")
        .json(&new_todo)
        .send()
        .await?
        .json()
        .await?;

    println!("{:#?}", create_todo);

    // Send json request with serde_json
    let json_todo: ToDo = Client::new()
        .post("https://jsonplaceholder.typicode.com/todos")
        .json(&serde_json::json!({
            "userId": 1,
            "title": "Hello world #2!".to_owned(),
            "completed": true
        }))
        .send()
        .await?
        .json()
        .await?;

    println!("{:#?}", json_todo);


    Ok(())
}
